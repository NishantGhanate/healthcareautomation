from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

import json
import cv2
import numpy as np
import base64 
import time

from django.contrib.auth.decorators import login_required
from user.models import Patient,Details

# Create your views here.
@login_required
def dashboard(request):
    return render(request,'user/dashboard.html')


def data_uri_to_cv2_img(uri):
    # encoded_data = uri.split(',')[1]
    encoded_data = uri
    nparr = np.fromstring(base64.b64decode(encoded_data), np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img

def newPatient(request):
    if request.method =="POST":
        get_value= request.body[6:]
        print(request.body[:20])
        # Do your logic here coz you got data in `get_value`
        data = {}
        # data['result'] = 'Serve'
        inputImage = data_uri_to_cv2_img(get_value)
        qrDecoder = cv2.QRCodeDetector()
        print(inputImage)
        # message,bbox,rectifiedImage = qrDecoder.detectAndDecode(inputImage)
        if len(data)>0:
            data['result'] = message
        else:
            data['result'] = 'Could not Detect QR Code'
        # Detect and decode the qrcode
        t = time.time()

        return HttpResponse(json.dumps(data), content_type="application/json")
    elif request.method == 'GET':
        return render(request, 'user/newPatient.html')
        

def about(request):
    return render(request,'user/about.html')

@login_required
def profile(request):
    return render(request, 'user/profile.html')

class DoctorsList(ListView):
    model=Details
    template_name='user/doctors.html'
    context_object_name='details'

class PatientList(ListView):
    model=Patient
    template_name='user/patients.html'
    context_object_name='patients'

class HistoryList(ListView):
    model=Details
    template_name='user/history.html'
    context_object_name='details'

def decoder(request):
    return render(request,'user/test/Working/example1.html')
  
